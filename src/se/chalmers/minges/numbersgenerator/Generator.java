/*
 * ----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <florianminges@gmail.com> wrote this file. As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a soft drink in return Florian Minges
 * ----------------------------------------------------------------------------
 */

package se.chalmers.minges.numbersgenerator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Support class for generating numbers.
 * 
 * @author Florian Minges
 */
public final class Generator {
	
	/**
	 * Hidden private constructor.
	 */
	private Generator() {}
	
	/**
	 * Generates a shuffled <code>List</code> of numbers in Stringformat 
	 * from 0 to <code>amount</code>, excluding <code>amount</code>.<p>
	 * 
	 * @param amount the amount of numbers to generate
	 * @return a shuffled <code>List</code> of numbers
	 */
	public static List<String> generateNumbers(int amount) {
		List<String> numbers = new ArrayList<String>();
		int precision = String.valueOf(amount - 1).length();
		for (int number = 0; number < amount; number++) {
			numbers.add(toString(number, precision));
		}
		
		Collections.shuffle(numbers); //shuffle random order
		return numbers;
	}
	
	/**
	 * Returns a string representation of a number, with
	 * a certain precision provided in the parameter.<p>
	 * The precision tells how many digits the number should contain,
	 * and if the number is to small it appends zeros to the beginning 
	 * of the string.
	 * 
	 * @param number the number to convert
	 * @param precision the number of digits the string should contain
	 * @return a string representation of a number
	 */
	private static String toString(int number, int precision) {
		StringBuilder builder = new StringBuilder("");
		int missingZeros = precision - String.valueOf(number).length();
		for (int i = 0; i < missingZeros; i++) {
			builder.append(0);
		}
		builder.append(number);
		
		return builder.toString();
	}

}
