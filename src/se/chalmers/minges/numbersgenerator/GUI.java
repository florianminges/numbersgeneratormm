/*
 * ----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <florianminges@gmail.com> wrote this file. As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a soft drink in return Florian Minges
 * ----------------------------------------------------------------------------
 */

package se.chalmers.minges.numbersgenerator;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JSpinner;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;

import se.chalmers.minges.iosupport.Write;

public class GUI extends JFrame {
	
	private final Dimension WINDOW_SIZE = new Dimension(200, 150);
	private final String FILE_PATH = "numbers.txt";
	
	private SpinnerModel spinnerModel;
	private String message;
	
	public static void main(String [] args) {
		new GUI();
	}
	
	/**
	 * Constructs a new GUI for this application.
	 */
	public GUI() {
		super();
		initializeContent();
		initializeMessage();
		
		setSize(WINDOW_SIZE);
		setPreferredSize(WINDOW_SIZE);
		pack();
		setLocationRelativeTo(null);
		setVisible(true);
	}
	
	private void initializeContent() {
		//create spinner
		this.spinnerModel = new SpinnerNumberModel(100, 10, 10000, 50);
		JSpinner spinner = new JSpinner(spinnerModel);
		
		//create button
		JButton button = new JButton("Generate file");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				List<String> list = Generator.generateNumbers((Integer) spinnerModel.getValue());
				Write.saveToFile(Write.createMultiLineString(list), FILE_PATH);
				JOptionPane.showMessageDialog(GUI.this, message);
			}
		});
	
		getContentPane().setLayout(new GridLayout(2, 1));
		getContentPane().add(spinner);
		getContentPane().add(button);
	}
	
	private void initializeMessage() {
		StringBuilder builder = new StringBuilder();
		builder.append("Your file was created successfully!\n");
		builder.append("It should have been saved in the same folder as this program\n");
		builder.append("under the name \"numbers.txt\".\n");
		builder.append("Now you just have to copy the file to the right directory,\n");
		builder.append("and change a small setting and you are ready to go. Good luck!");
		this.message = builder.toString();
	}
}
